/*
 * Aniela Tatowicz
 * Jacek Kubiak
 * �roda 18.55
 */

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.EmptyBorder;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoundedRangeModel;
import javax.swing.ImageIcon;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.Timer;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class Snake extends JFrame implements ActionListener, KeyListener {

	private JPanel contentPane;
	
	public static JFrame jframe;
	
	public static Snake snake;
	public RenderPanel renderPanel;
	public ArrayList<Point> snakeParts = new ArrayList<Point>();
	public static final int UP = 0, DOWN = 1, LEFT = 2, RIGHT = 3, SCALE = 10;
	public int ticks = 0, direction = DOWN, score, tailLength = 10, time;
	public Point head, cherry;
	public Random random;
	public boolean over = false, paused;
	public Dimension dim;
	public Image imageicon;
	
	static final int TIMER_MIN = 10;
	static final int TIMER_MAX = 70;
	static final int TIMER_INIT = 40; 
	static int timeDelay = TIMER_INIT;
	public Timer timer = new Timer(timeDelay, this);

	public static void main(String[] args) {	
		snake = new Snake();
		snake.addKeyListener(snake);
		snake.setVisible(true);
		snake.setResizable(false);
        snake.setFocusable(true);
        snake.setFocusTraversalKeysEnabled(false);
	}

	public Snake() { 
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 750);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		RenderPanel renderPanel_1 = new RenderPanel();
		renderPanel_1.setBounds(10, 10, 800, 700);
		contentPane.add(renderPanel_1);
		
		JButton btnStart = new JButton("Pauza");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(paused)
				{
					paused=false;
					btnStart.setText("Pauza");
				}
				else
				{
					paused=true;
					btnStart.setText("Start");
					contentPane.repaint();
				}
			}
			
		});
		btnStart.setBounds(841, 22, 89, 23);
		btnStart.setFocusable(false);
		contentPane.add(btnStart);
		
		JButton btnRestart = new JButton("Restart");
		btnRestart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				startGame();
				btnStart.setText("Pauza");
			}
		});
		btnRestart.setBounds(841, 62, 89, 23);
		btnRestart.setFocusable(false);
		contentPane.add(btnRestart);
		
		JSlider slider = new JSlider(JSlider.VERTICAL, TIMER_MIN, TIMER_MAX, TIMER_INIT);
		slider.setMinorTickSpacing(2);
		slider.setMajorTickSpacing(10);
	    slider.setPaintTicks(true);
	    
	    ChangeListener aChangeListener = new BoundedChangeListener();
	    BoundedRangeModel model = slider.getModel();
	    model.addChangeListener(aChangeListener);
	    slider.addChangeListener(aChangeListener);
	    
		slider.setBounds(841, 116, 36, 200);
		slider.setFocusable(false);
		contentPane.add(slider);
		
		JLabel lblPrdko = new JLabel("Pr\u0119dko\u015B\u0107");
		lblPrdko.setBounds(841, 96, 68, 14);
		contentPane.add(lblPrdko);
		
		JLabel lblSlow = new JLabel("Slow");
		lblSlow.setBounds(884, 117, 46, 14);
		contentPane.add(lblSlow);
		
		JLabel lblFast = new JLabel("Fast & Furious");
		lblFast.setBounds(887, 299, 95, 17);
		contentPane.add(lblFast);
		
		JLabel panel = new JLabel();
		panel.setBounds(875, 427, 107, 87);
		panel.setIcon(new ImageIcon("src/wonsz.gif"));
		contentPane.add(panel);
		
		addKeyListener(snake);
		startGame();
		
	}
	
	class BoundedChangeListener implements ChangeListener 
	{
		public void stateChanged(ChangeEvent changeEvent) 
		{
			Object source = changeEvent.getSource();
		    if (source instanceof BoundedRangeModel) 
		    {
		    	BoundedRangeModel aModel = (BoundedRangeModel) source;
		    	timeDelay=aModel.getValue();
		    	timer.setDelay(timeDelay); 	       
		    }      
		 }
	}
	
	public void startGame()
	{
		over = false;
		paused = false;
		time = 0;
		score = 0;
		tailLength = 10;
		ticks = 0;
		direction = DOWN;
		head = new Point(0, -1);
		random = new Random();
		snakeParts.clear();
		cherry = new Point(random.nextInt(79), random.nextInt(66));		
		timer.start();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		ticks++;

		if (ticks % 2 == 0 && head != null && !over && !paused)
		{
			time+=timeDelay;
			snakeParts.add(new Point(head.x, head.y));

			if (direction == UP)
			{
				if (head.y - 1 >= 0)
				{
					if(!noTailAt(head.x, head.y - 1)) {
						over = true;
					}
					head = new Point(head.x, head.y - 1);
				}
				else
				{
					if(!noTailAt(head.x, head.y + 69)) {
						over = true;
					}
					head = new Point(head.x, head.y + 69);
				}
			}

			if (direction == DOWN)
			{
				if (head.y + 1 < 70)
				{
					if(!noTailAt(head.x, head.y + 1)) {
						over = true;
					}
					head = new Point(head.x, head.y + 1);
				}
				else
				{
					if(!noTailAt(head.x, head.y -69)) {
						over = true;
					}
					head = new Point(head.x, head.y - 69);
				}
			}

			if (direction == LEFT)
			{
				if (head.x - 1 >= 0)
				{
					if(!noTailAt(head.x - 1, head.y)) {
						over = true;
					}
					head = new Point(head.x - 1, head.y);
				}
				else
				{
					if(!noTailAt(head.x + 79, head.y)) {
						over = true;
					}
					head = new Point(head.x + 79, head.y);
				}
			}

			if (direction == RIGHT)
			{
				if (head.x + 1 < 80)
				{
					if(!noTailAt(head.x + 1, head.y)) {
						over = true;
					}
					head = new Point(head.x + 1, head.y);
				}
				else
				{
					if(!noTailAt(head.x - 79, head.y)) {
						over = true;
					}
					head = new Point(head.x - 79, head.y);
				}
			}

			if (snakeParts.size() > tailLength)
			{
				snakeParts.remove(0);
			}

			if (head.equals(cherry))
			{
				score += 10;
				tailLength++;
				do {
					cherry.x = random.nextInt(79);
					cherry.y = random.nextInt(66);
				} while(!noTailAt(cherry.x,cherry.y));
				cherry.setLocation(cherry.x, cherry.y);
			}
			contentPane.repaint();
		}
	}
	
	@Override
	public void keyPressed(KeyEvent e)
	{
		if(!paused) {
			int i = e.getKeyCode();
	
			if ((i == KeyEvent.VK_A || i == KeyEvent.VK_LEFT) && direction != RIGHT)
			{
				direction = LEFT;
			}
	
			if ((i == KeyEvent.VK_D || i == KeyEvent.VK_RIGHT) && direction != LEFT)
			{
				direction = RIGHT;
			}
	
			if ((i == KeyEvent.VK_W || i == KeyEvent.VK_UP) && direction != DOWN)
			{
				direction = UP;
			}
	
			if ((i == KeyEvent.VK_S || i == KeyEvent.VK_DOWN) && direction != UP)
			{
				direction = DOWN;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
	}
	
	public boolean noTailAt(int x, int y)
	{
		for (Point point : snakeParts)
		{
			if (point.equals(new Point(x, y)))
			{
				return false;
			}
		}
		return true;
	}
}
